Attached Bundles module by eLogic
=====================

Short description
---------------------

Attached Bundles module for Magento 2 helps you to show bundles for simple and configuration product on PDP.

Install
---------------------
To install the module you need to clone repository into your magento 2 application folder "your-app-folder-name/app/code/Elogic/BundleAttachedToSimple/".
For example (using https):
```sh
$ cd clone <your-app-folder-name>/app/code/
$ git clone https://gitlab.com/elogic-group/attached-bundles Elogic/BundleAttachedToSimple/
```
or just simply download it from site and unpack in the same way.

### Upgrade magento modules
The next step is to upgrade your application in order to register the module in magento 2 system. But before that you need to check this for sure:
```sh
$ ./bin/magento setup:db:status
```
So, if there is a need you have to go on the root folder of Magento 2 (cd your-app-folder-name) and type:
```sh
$ ./bin/magento setup:upgrade
```

How to use "Attached Bundles" module
---------------------

Log in to your admin panel and choose PRODUCTS->CATALOG, choose a product you want and press "edit" button on the right hand, then go to the "Attached Bundles" section.

![img_2.png](screenshots/img_2.png)

After this you will see a button "Add attached bundles". If you press this button you will see all bundles which include your simple or configurable product.

![img_3.png](screenshots/img_3.png)

You can choose bundle products which you want to show on product page and press "Add selected products" button.

![img_4.png](screenshots/img_4.png)

You can sort bundle products by position.
![img_5.png](screenshots/img_5.png)

Bundle Product which is at first place will have the highest priority.


<?php
namespace Elogic\BundleAttachedToSimple\Plugin;

class UpdateToAttachedBundlesModel
{
    /**
     * @param \Elogic\BundleAttachedToSimple\Model\ProductFactory $catalogModel
     */
    public function __construct(
        \Elogic\BundleAttachedToSimple\Model\ProductFactory $catalogModel
    ) {
        $this->catalogModel = $catalogModel;
    }

    /**
     * Before plugin to update model class
     *
     * @param \Elogic\BundleAttachedToSimple\Model\ProductLink\CollectionProvider\AttachedBundlesProducts $subject
     * @param Object $product
     * @return array
     */
    public function beforeGetLinkedProducts(
        \Elogic\BundleAttachedToSimple\Model\ProductLink\CollectionProvider\AttachedBundlesProducts $subject,
        $product
    ) {
        $currentProduct = $this->catalogModel->create()->load($product->getId());
        return [$currentProduct];
    }
}

<?php
namespace Elogic\BundleAttachedToSimple\Model\ProductLink\CollectionProvider;

class AttachedBundlesProducts
{
    public function getLinkedProducts($product)
    {
        return $product->getAttachedBundlesProducts();
    }
}

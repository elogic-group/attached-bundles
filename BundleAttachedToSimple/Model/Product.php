<?php
namespace Elogic\BundleAttachedToSimple\Model;

class Product extends \Magento\Catalog\Model\Product
{
    const LINK_TYPE = 'attached_bundles';
    const LINK_TYPE_CUSTOMLINK = 7;

    /**
     * Retrieve array of customlink products
     *
     * @return array
     */
    public function getAttachedBundlesProducts()
    {
        if (!$this->hasAttachedBundlesProducts()) {
            $products = [];
            $collection = $this->getAttachedBundlesProductCollection();
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setAttachedBundlesProducts($products);
        }
        return $this->getData('attached_bundles_products');
    }

    /**
     * Retrieve attached_bundles products identifiers
     *
     * @return array
     */
    public function getAttachedBundlesProductIds()
    {
        if (!$this->hasAttachedBundlesProductIds()) {
            $ids = [];
            foreach ($this->getAttachedBundlesProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setAttachedBundlesProductIds($ids);
        }
        return [$this->getData('attached_bundles_product_ids')];
    }

    /**
     * Retrieve collection attached_bundles product
     *
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getAttachedBundlesProductCollection()
    {
        $collection = $this->getLinkInstance()->setLinkTypeId(
            static::LINK_TYPE_CUSTOMLINK
        )->getProductCollection()->setIsStrongMode();
        $collection->setProduct($this);

        return $collection;
    }
}

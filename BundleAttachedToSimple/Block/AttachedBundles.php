<?php

namespace Elogic\BundleAttachedToSimple\Block;

use Magento\Framework\View\Element\Template;
use Magento\Bundle\Model\Product\Type;
use Magento\Framework\Registry;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogWidget\Block\Product\ProductsList;
use Magento\Catalog\Model\ProductLink\CollectionProvider;
use Elogic\BundleAttachedToSimple\Model\Product;

class AttachedBundles extends Template
{
    const PRODUCT_TYPE_SIMPLE = 'simple';
    const PRODUCT_TYPE_CONFIGURABLE = 'configurable';

    /**
     * @var Type
     */
    protected $bundleProductType;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * @var ProductsList
     */
    protected $productsList;

    /**
     * @var CollectionProvider
     */
    protected $collectionProvider;

    /**
     * @param Type $bundleProductType
     * @param Registry $registry
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CollectionProvider $collectionProvider,
        Type $bundleProductType,
        Registry $registry,
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        ProductsList $productsList,
        array $data = []
    ){
        $this->collectionProvider = $collectionProvider;
        $this->bundleProductType = $bundleProductType;
        $this->registry = $registry;
        $this->productRepository = $productRepository;
        $this->productsList = $productsList;
        parent::__construct($context, $data);
    }

    public function getBundleProductsData()
    {
        $collection = $this->getAttachedBundlesCollection();
        $productData = [];
        //$bundleIds = $this->getParentItemsIds();

       foreach ($collection as $item) {
           $product = $this->productRepository->get($item['sku']);
           $productData[] = [
               "id"            => $product->getId(),
               "name"          => $product->getName(),
               "url"           => $product->getProductUrl(),
               "image"         => $product->getImage() ? $product->getMediaConfig()->getMediaUrl($product->getImage()) : '',
               "final_price"   => $product->getPriceInfo()->getPrice('final_price')->getValue(),
               "minimal_price" => $product->getPriceInfo()->getPrice('final_price')->getMinimalPrice()->getValue(),
               "maximum_price" => $product->getPriceInfo()->getPrice('final_price')->getMaximalPrice()->getValue(),
               "position"      => $item['position']
           ];
       }

       return $productData;
    }

    public function checkProductIsSimpleOrConfigurable()
    {
        $productType = $this->registry->registry('current_product')->getTypeId();
        if ($productType === self::PRODUCT_TYPE_SIMPLE || $productType === self::PRODUCT_TYPE_CONFIGURABLE) {
            return true;
        }

        return false;
    }

    public function getAddToCartUrl($product)
    {
        return $this->productsList->getAddToCartUrl($product);
    }

    public function getBundleProduct($id)
    {
        return $this->productRepository->getById($id);
    }

    public function getAttachedBundlesCollection()
    {
        $product = $this->registry->registry('current_product');
        return $this->collectionProvider->getCollection($product, Product::LINK_TYPE);
    }
}
